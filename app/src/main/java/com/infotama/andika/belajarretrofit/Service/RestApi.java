package com.infotama.andika.belajarretrofit.Service;

import com.infotama.andika.belajarretrofit.Models.Models;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by andika on 9/8/16.
 */
public interface RestApi {
    @GET("contohjson")
    Call<Models> getDataAdmin();
}
