package com.infotama.andika.belajarretrofit;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.v4.widget.TextViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.infotama.andika.belajarretrofit.Models.Models;
import com.infotama.andika.belajarretrofit.Service.RestApi;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private TextView namaText;
    private TextView emailText;
    private TextView alamatText;
    private TextView statusText;

    public static final String ROOT_URL = "http://api.teknorial.com/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        namaText = (TextView)findViewById(R.id.nama_text);
        emailText = (TextView)findViewById(R.id.email_text);
        alamatText = (TextView)findViewById(R.id.alamat_text);
        statusText = (TextView)findViewById(R.id.status_text);

        getData();
    }

    private void getData(){
        final ProgressDialog loading = new ProgressDialog(this);
        //.show(this, "Fetching Data","Please wait..",false,false);
        loading.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestApi service = retrofit.create(RestApi.class);
        Call<Models> call = service.getDataAdmin();
        call.enqueue(new Callback<Models>() {
            @Override
            public void onResponse(Call<Models> call, Response<Models> response) {
                try {
                    loading.dismiss();
                    String id = response.body().getAdmin().getId().toString();
                    String nama = response.body().getAdmin().getNama();
                    String alamat = response.body().getAdmin().getAlamat();
                    String email = response.body().getAdmin().getEmail();
                    String status = response.body().getAdmin().getStatus();

                    namaText.setText("Nama :" + nama);
                    emailText.setText("nama :"+ email);
                    alamatText.setText("Alamat:" + alamat);
                    statusText.setText("Status:" + status);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Models> call, Throwable t) {

            }
        });
    }
}
